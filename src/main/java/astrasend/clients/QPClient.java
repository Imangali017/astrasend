package astrasend.clients;

import astrasend.model.dtos.abis.AbisMoneyPayRequest;
import astrasend.model.dtos.abis.AbisMoneyPayResponse;
import astrasend.model.dtos.EsbRequest;
import astrasend.model.dtos.EsbRequestHeader;
import astrasend.model.dtos.EsbResponse;
import astrasend.model.dtos.ReceiveMoneySearchRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class QPClient {

    private static final Logger log = LoggerFactory.getLogger(astrasend.clients.EsbClient.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final RestTemplate restTemplate;

    @Value("${bas.qp.url}")
    private String qPBaseUrl;

    public QPClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public EsbResponse<AbisMoneyPayResponse> doReceiveMoneySearch(
        AbisMoneyPayRequest abisMoneyPayRequest) {
//        var esbUrl = String.format(qPBaseUrl + "/receive-money-search-request");
//        EsbRequest<AbisMoneyPayRequest> esbRequest = new EsbRequest<>(
//            new EsbRequestHeader("AstraFico", "Astrasend"),
//                abisMoneyPayRequest);
//        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
//        });
        EsbResponse<AbisMoneyPayResponse> esbResponse = new EsbResponse<>();
        AbisMoneyPayResponse abisMoneyPayResponse = new AbisMoneyPayResponse();
        abisMoneyPayResponse.setKndp(abisMoneyPayRequest.getKndp());
        abisMoneyPayResponse.setOperation("123456");
        abisMoneyPayResponse.setStatus("ФСА");
        esbResponse.setRsParms(abisMoneyPayResponse);
        return esbResponse;
    }

    private <T> EsbResponse<T> doRequest(String url, EsbRequest request,
        ParameterizedTypeReference<EsbResponse<T>> responseType) {
        try {
            var httpEntity = new HttpEntity<>(request);
            log.info("QP {}", request);
            ResponseEntity<EsbResponse<T>> response = restTemplate.exchange(url, HttpMethod.POST,
                httpEntity, responseType);
            logObjectAsJson("QP: ", response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            try {
                var errorResponse = objectMapper.readValue(e.getResponseBodyAsString(),
                    EsbResponse.class);
                log.info("QP row {}", e.getResponseBodyAsString());
                return errorResponse;
            } catch (JsonProcessingException jsonMappingException) {
                log.error("Could not convert error response", jsonMappingException);
                throw new RuntimeException("Could not convert error response from QP");
            }
        }
    }

    private void logObjectAsJson(String txt, Object o) {
        try {
            log.info("{} {}", txt, objectMapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            log.warn("error logging", e);
        }
    }


}
