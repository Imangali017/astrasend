package astrasend.clients;

import astrasend.model.dtos.EsbRequest;
import astrasend.model.dtos.EsbRequestHeader;
import astrasend.model.dtos.EsbResponse;
import astrasend.model.dtos.ReceiveMoneyCancelRequest;
import astrasend.model.dtos.ReceiveMoneyCancelResponse;
import astrasend.model.dtos.ReceiveMoneyPayReply;
import astrasend.model.dtos.ReceiveMoneyPayRequest;
import astrasend.model.dtos.ReceiveMoneySearchReply;
import astrasend.model.dtos.ReceiveMoneySearchRequest;
import astrasend.model.dtos.ReceiveMoneyStatusRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * Esb url on which requests were tested http://172.16.99.66:7801
 *
 * @author Gnedko Vyacheslav
 * @since 25.09.2021 19:16
 */
@Service
public class EsbClient {

    private static final Logger log = LoggerFactory.getLogger(EsbClient.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final RestTemplate restTemplate;

    @Value("${bas.esb.url}")
    private String esbBaseUrl;

    public EsbClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public EsbResponse<ReceiveMoneySearchReply> doReceiveMoneySearch(
            ReceiveMoneySearchRequest receiveMoneySearchRequest) {
        var esbUrl = String.format(esbBaseUrl + "/astrasend/receiveMoneySearch");
        EsbRequest<ReceiveMoneySearchRequest> esbRequest = new EsbRequest<>(
                new EsbRequestHeader("receive-money-search", "Astrasend"),
                receiveMoneySearchRequest);
        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
        });
    }

    public EsbResponse<ReceiveMoneySearchReply> doCancelMoneySearch(
            ReceiveMoneySearchRequest receiveMoneySearchRequest) {
        var esbUrl = String.format(esbBaseUrl + "/astrasend/searchRequest");
        EsbRequest<ReceiveMoneySearchRequest> esbRequest = new EsbRequest<>(
                new EsbRequestHeader("search-request", "Astrasend"),
                receiveMoneySearchRequest);
        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
        });
    }

    public EsbResponse<ReceiveMoneyPayReply> doReceiveMoneyPay(ReceiveMoneyPayRequest receiveMoneyPayRequest) {
        var esbUrl = String.format(esbBaseUrl + "/astrasend/receiveMoneyPay");
        EsbRequest<ReceiveMoneyPayRequest> esbRequest = new EsbRequest<>(
                new EsbRequestHeader("receive-money-pay", "Astrasend"), receiveMoneyPayRequest);
        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
        });
    }

    public EsbResponse<ReceiveMoneyCancelResponse> doCancelMoneyPay(ReceiveMoneyCancelRequest receiveMoneyPayRequest) {
        var esbUrl = String.format(esbBaseUrl + "/astrasend/cancelPaidRequest");
        EsbRequest<ReceiveMoneyCancelRequest> esbRequest = new EsbRequest<>(
                new EsbRequestHeader("cancel-paid-request", "Astrasend"), receiveMoneyPayRequest);
        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
        });
    }

    public EsbResponse<ReceiveMoneySearchReply> doReceivePayStatusInquiry(
            ReceiveMoneyStatusRequest receiveMoneySearchRequest) {
        var esbUrl = String.format(esbBaseUrl + "/astrasend/payStatusInquiry");
        EsbRequest<ReceiveMoneyStatusRequest> esbRequest = new EsbRequest<>(
                new EsbRequestHeader("pay-status-inquiry-request-data", "Astrasend"),
                receiveMoneySearchRequest);
        return doRequest(esbUrl, esbRequest, new ParameterizedTypeReference<>() {
        });
    }

    private <T> EsbResponse<T> doRequest(String url, EsbRequest request,
                                         ParameterizedTypeReference<EsbResponse<T>> responseType) {
        try {
            var httpEntity = new HttpEntity<>(request);
            log.info("EsbRq {}", request);
            ResponseEntity<EsbResponse<T>> response = restTemplate.exchange(url, HttpMethod.POST,
                    httpEntity, responseType);
            logObjectAsJson("EsbRs: ", response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            try {
                var errorResponse = objectMapper.readValue(e.getResponseBodyAsString(),
                        EsbResponse.class);
                log.info("EsbRs row {}", e.getResponseBodyAsString());
                return errorResponse;
            } catch (JsonProcessingException jsonMappingException) {
                log.error("Could not convert error response", jsonMappingException);
                throw new RuntimeException("Could not convert error response from Esb");
            }
        }
    }

    private void logObjectAsJson(String txt, Object o) {
        try {
            log.info("{} {}", txt, objectMapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            log.warn("error logging", e);
        }
    }


}
