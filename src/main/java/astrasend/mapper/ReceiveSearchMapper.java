package astrasend.mapper;

import astrasend.model.dtos.PaymentTransaction;
import astrasend.model.entity.Transfer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReceiveSearchMapper {

    @Mapping(target = "firstName", source = "sender.name.firstName")
    @Mapping(target = "lastName", source = "sender.name.lastName")
    @Mapping(target = "middleName", source = "sender.name.middleName")
    @Mapping(target = "dateOfBirth", source = "sender.complianceDetails.dateOfBirth")
    @Mapping(target = "senderCountry", source = "sender.complianceDetails.currentAddress.country")
    @Mapping(target = "postalCode", source = "sender.complianceDetails.currentAddress.postalCode")
    @Mapping(target = "region", source = "sender.complianceDetails.currentAddress.region")
    @Mapping(target = "district", source = "sender.complianceDetails.currentAddress.city")
    @Mapping(target = "addressLine1", source = "sender.complianceDetails.currentAddress.addrLine1")
    @Mapping(target = "paidDate", ignore = true) //TODO set after mapping
    @Mapping(target = "originatingCity", source = "paymentDetails.originatingCity")
    @Mapping(target = "destinationCity", source = "paymentDetails.destinationCity")
    @Mapping(target = "exchangeRate", source = "paymentDetails.exchangeRate")
    @Mapping(target = "originCountry", source = "paymentDetails.originatingCountryCurrency.isoCode.countryCode")
    @Mapping(target = "originCurrencyCode", source = "paymentDetails.originatingCountryCurrency.isoCode.currencyCode")
    @Mapping(target = "destinationCountry", source = "paymentDetails.originalDestinationCountryCurrency.isoCode.countryCode")
    @Mapping(target = "destinationCurrencyCode", source = "paymentDetails.originalDestinationCountryCurrency.isoCode.countryCode")
    @Mapping(target = "idType", source = "sender.complianceDetails.idDetails.idType")
    @Mapping(target = "idSeries", source = "sender.complianceDetails.idDetails.idSeries")
    @Mapping(target = "idIssueDate", source = "sender.complianceDetails.idDetails.idIssueDate")
    @Mapping(target = "idCountryOfIssue", source = "sender.complianceDetails.idDetails.idCountryOfIssue")
    @Mapping(target = "idPlaceOfIssue", source = "sender.complianceDetails.idDetails.idPlaceOfIssue")
    @Mapping(target = "idExpirationDate", source = "sender.complianceDetails.idDetails.idExpirationDate")
    @Mapping(target = "isAResident", source = "sender.complianceDetails.isAResident")
    @Mapping(target = "taxPayersRegistrationNumber", source = "sender.complianceDetails.taxPayersRegistrationNumber")
    @Mapping(target = "otherPurposeOfTransaction", source = "sender.complianceDetails.otherPurposeOfTransaction")
    @Mapping(target = "beneficiaryCode", source = "sender.complianceDetails.beneficiaryCode")
    @Mapping(target = "senderResidencyCode", ignore = true)
    @Mapping(target = "transactionReason", ignore = true)
    @Mapping(target = "emailAddress", source = "sender.email")
    @Mapping(target = "nationalNumber", source = "sender.contactPhone")
    @Mapping(target = "payStatusDescription", source = "payStatusDescription")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "mtcn", source = "mtcn")
    @Mapping(target = "QPStatus", ignore = true)
    @Mapping(target = "finalStatus", ignore = true)
    @Mapping(target = "numberTransactionQP", ignore = true)
    @Mapping(target = "payAmount", source = "financials.payAmount")
    @Mapping(target = "receiverFirstName", source = "receiver.name.firstName")
    @Mapping(target = "receiverLastName", source = "receiver.name.lastName")
    @Mapping(target = "receiverMiddleName", source = "receiver.name.middleName")
    @Mapping(target = "newMtcn", source = "newMtcn")
    Transfer toEntity(PaymentTransaction paymentTransaction);
}
