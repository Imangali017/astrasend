package astrasend.mapper;

import astrasend.model.dtos.TransferDto;
import astrasend.model.entity.Transfer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TransferMapper {

    Transfer toEntity(TransferDto dto);

    @Mapping(target = "newMtcn", source = "newMtcn")
    @Mapping(target = "referenceNumber", source = "referenceNumber")
    TransferDto toDto(Transfer entity);

    List<Transfer> toEntity(List<TransferDto> dtoList);

    List<TransferDto> toDto(List<Transfer> entityList);
}
