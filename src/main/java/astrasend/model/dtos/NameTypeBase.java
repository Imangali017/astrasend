package astrasend.model.dtos;

import astrasend.model.enums.NameType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NameTypeBase implements Serializable {
    private NameType nameType;
}
