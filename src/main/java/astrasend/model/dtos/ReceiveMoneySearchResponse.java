package astrasend.model.dtos;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneySearchResponse implements Serializable {
    private String mtcn;
    private Sender sender;
    private Receiver receiver;
    private String finalStatus;
    private BigDecimal payAmount;
    private String currency;
    private String senderCountry;
}
