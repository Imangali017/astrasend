package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneyStatusRequest implements Serializable {
    private PaymentDetails paymentDetails;
    private ForeignRemoteSystem foreignRemoteSystem;
    private String mtcn;
}
