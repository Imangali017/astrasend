package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BankDetails implements Serializable {
    private String name;
    private String accountNumber;
    private String routingNumber;
    private String accountType;
}
