package astrasend.model.dtos;

import astrasend.model.enums.FinalStatus;
import astrasend.model.enums.PaymentType;
import astrasend.model.enums.YesNo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransferDto implements Serializable {
    private Long id;
    private String firstName; //Фамилия отправителя
    private String lastName; //Имя отправителя
    private String middleName; //Отчество (при наличии) отправителя
    private String dateOfBirth; //Дата рождения отправителя
    private String senderCountry; //Страна
    private String postalCode; //Индекс
    private String region; //Область/Регион
    private String district; //Населенный пункт
    private String addressLine1; //Полный Адрес
    private String addressLine2;
    private LocalDateTime paidDate; //Дата и время отправки
    private String originatingCity; //Пункт отправки
    private String destinationCity; //Пункт получателя
    private String exchangeRate; //Курс обмена валют
    private String originCountry; //Страна отправки
    private String originCurrencyCode; //Валюта отправки
    private String destinationCountry; //Страна получателя
    private String destinationCurrencyCode; //Валюта получателя
    private String idType; //Тип ДУЛ Вручную сейчас Кассиры выбирают из справочника
    private String idSeries; //Серия и номер ДУЛ
    private String idIssueDate; //Дата выдачи ДУЛ
    private String idCountryOfIssue; //Орган, выдавший ДУЛ
    private String idPlaceOfIssue; //Страна выдачи ДУЛ
    private String idExpirationDate; //Дата окончания срока действия ДУЛ
    private String isAResident; //Является ли клиент резидентом
    private String taxPayersRegistrationNumber; //ИИН
    private String otherPurposeOfTransaction; //Назначение перевода
    private String beneficiaryCode; //КНП
    private String senderResidencyCode; //КОД
    private String fundsSenderCode; //КБЕ
    private String transactionReason; //Цель перевода
    private String emailAddress; //Адрес электронной почты
    private String nationalNumber; //Номер телефона
    private String payStatusDescription;
    private String mtcn;
    private String newMtcn;
    private String qPStatus;
    private FinalStatus finalStatus;
    private String numberTransactionQP;
    private BigDecimal payAmount;
    private String receiverFirstName;
    private String receiverLastName;
    private String receiverMiddleName;
    private Boolean doesTheIdHaveAnExpirationDate;
    private String countryOfBirth;
    private String personalIdentificationNumber;
    private String classifierOfThePurposeOfMtNumber;
    private String ackFlag;
    private Boolean marketingFlag;
    private PaymentType paymentType;
    private Boolean fixOnSend;
    private String operationId;
    private String departmentId;
    private String payOrDoNotPayIndicator;
    private String internationalPhoneNumberCountry;
    private IsoCode originalDestinationCountryCurrencyIsoCode;
    private String originalDestinationCountryCurrencyCountryName;
    private Long originatorsPrincipalAmount;
    private Long destinationPrincipalAmount;
    private String referenceNumber;

}
