package astrasend.model.dtos;

import astrasend.model.enums.PaymentType;
import astrasend.model.enums.YesNo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaymentDetails implements Serializable {
    private CountryCurrencyInfo originatingCountryCurrency;
    private CountryCurrencyInfo destinationCountryCurrency;
    private String originatingCity;
    private String destinationCity;
    private PaymentType paymentType;
    private Double exchangeRate;
    private YesNo fixOnSend;
    private CreditDebitCardDetails creditDebitCardDetails;
    private String duplicateDetectionFlag;
    private CountryCurrencyInfo originalDestinationCountryCurrency;
}
