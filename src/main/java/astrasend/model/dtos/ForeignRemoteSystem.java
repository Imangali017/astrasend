package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ForeignRemoteSystem implements Serializable {
    private String identifier;
    private String referenceNo;
    private String locationId;
    private String counterId;
    private String operatorId;
}
