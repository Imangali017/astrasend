package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Address implements Serializable {
    private String addrLine1;
    private String addrLine2;
    private String city;
    private String postalCode;
    private String country;
    private String okato;
    private String region;
    private String street;
    private String district;
    private String house;
    private String bld;
    private String appt;
}
