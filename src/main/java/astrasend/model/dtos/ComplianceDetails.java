package astrasend.model.dtos;

import astrasend.model.enums.YesNo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ComplianceDetails implements Serializable {
    private String templateId;
    private IdDetails idDetails;
    private IdDetails secondId;
    private String dateOfBirth;
    private Address currentAddress;
    private YesNo notAPep;
    private String occupation;
    private String transactionReason;
    private String countryOfBirth;
    private YesNo isAResident;
    private String citizenship;
    private String sourceOfFunds;
    private String cityOfBirth;
    private String personalIdentificationNumber;
    private String taxPayersRegistrationNumber;
    private String classifierOfThePurposeOfMtNumber;
    private String ackFlag;
    private String fundsSenderCode;
    private String beneficiaryCode;
    private String otherPurposeOfTransaction;
    private YesNo marketingFlag;
    private YesNo loyaltyFlag;
    private String riskTier;
}
