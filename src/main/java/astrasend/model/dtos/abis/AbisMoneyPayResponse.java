package astrasend.model.dtos.abis;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AbisMoneyPayResponse implements Serializable {

    private String op;
    private String operation;
    private String kndp;
    private String status;

}
