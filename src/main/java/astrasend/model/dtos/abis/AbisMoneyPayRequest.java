package astrasend.model.dtos.abis;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AbisMoneyPayRequest implements Serializable {
    private String kndp;
    private BigDecimal sum;
    private String currency;
    private String senderName;
    private String senderAddress;
    private String senderCountry;
    private String receiverName;
    private String document;
    private String docNum;
    private String docIssue;
    private String receiverInn;
    private String details;
    private String knp;
    private String kbe;
    private String kod;
    private String userId;
}
