package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EsbResponseHeader implements Serializable {
    private String service;
    private String platform;
    private String sourceSystem;
    private String rqUid;
    private String rsUid;
    private String operUid;
    private String rqTm;
    private String rsTm;
}
