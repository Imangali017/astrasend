package astrasend.model.dtos;

import astrasend.model.enums.NamePrefix;
import astrasend.model.enums.NameSuffix;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ChannelGeneralName implements Serializable {
    private NamePrefix namePrefix;
    private String firstName;
    private String middleName;
    private String lastName;
    private String cyrillicFirstName;
    private String cyrillicMiddleName;
    private String cyrillicLastName;
    private NameSuffix nameSuffix;
}
