package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EsbRequestHeader implements Serializable {

    private String service;

    private String platform;

    private String sourceSystem;

    private UUID rqUid;

    private UUID operUid;

    private String rqTm;
    private final String SOURCE_NAME = "Система-инициатор";

    public EsbRequestHeader(String service, String platform) {
        this.service = service;
        this.platform = platform;
        this.sourceSystem = SOURCE_NAME;
        this.rqUid = UUID.randomUUID();
        this.operUid = UUID.randomUUID();
        this.rqTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }
}
