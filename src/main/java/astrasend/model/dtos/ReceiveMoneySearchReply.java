package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneySearchReply extends TransactionBase implements Serializable {
    private PaymentTransactions paymentTransactions;
    private ErrorResponse errors;
    private ForeignRemoteSystem foreignRemoteSystem;
}
