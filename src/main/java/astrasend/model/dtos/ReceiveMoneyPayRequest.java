package astrasend.model.dtos;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReceiveMoneyPayRequest implements Serializable {
    private Receiver receiver;
    private PaymentDetails paymentDetails;
    private Financials financials;
    private String mtcn;
    private String newMtcn;
    private String payOrDoNotPayIndicator;
    private ForeignRemoteSystem foreignRemoteSystem;
}
