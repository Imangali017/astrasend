package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Financials implements Serializable {
    private Taxes taxes;
    private Long originatorsPrincipalAmount;
    private Long destinationPrincipalAmount;
    private Long grossTotalAmount;
    private Long plusChargesAmount;
    private Long payAmount;
    private Long principalAmount;
    private Long charges;
    private Long agentFee;
    private Long locationTransferLimit;
    private Long agentTransferLimit;
}
