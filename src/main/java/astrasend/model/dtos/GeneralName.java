package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GeneralName extends NameTypeBase implements Serializable {
    private String firstName;
    private String middleName;
    private String lastName;
    private String cyrillicFirstName;
    private String cyrillicMiddleName;
    private String cyrillicLastName;
}
