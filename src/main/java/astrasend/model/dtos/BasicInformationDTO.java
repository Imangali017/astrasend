package astrasend.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BasicInformationDTO {
    @JsonProperty("first_name")
    private String firstName; //Фамилия
    @JsonProperty("last_name")
    private String lastName; //Имя
    @JsonProperty("middle_name")
    private String middleName; //Отчество (при наличии)
    @JsonProperty("date_of_birth")
    private String dateOfBirth; //Дата рождения
    @JsonProperty("country_of_birth")
    private String countryOfBirth; //Страна рождения
    @JsonProperty("place_of_birth")
    private String country; //Место рождения
    @JsonProperty("id_type")
    private String idType; //Тип ДУЛ Вручную сейчас Кассиры выбирают из справочника
    @JsonProperty("id_series")
    private String idSeries; //Серия и номер ДУЛ
    @JsonProperty("id_issue_date")
    private String idIssueDate; //Дата выдачи ДУЛ
    @JsonProperty("id_country_of_issue")
    private String idCountryOfIssue; //Орган, выдавший ДУЛ
    @JsonProperty("id_place_of_issue")
    private String idPlaceOfIssue; //Страна выдачи ДУЛ
    @JsonProperty("id_expiration_date")
    private String idExpirationDate; //Дата окончания срока действия ДУЛ
    @JsonProperty("is_a_resident")
    private String isAResident; //Является ли клиент резидентом
    @JsonProperty("tax_payers_registration_number")
    private String taxPayersRegistrationNumber; //ИИН
    @JsonProperty("other_purpose_of_transaction")
    private String otherPurposeOfTransaction; //Назначение перевода
    @JsonProperty("beneficiary_code")
    private String beneficiaryCode; //КНП
    @JsonProperty("funds_sender_code")
    private String senderResidencyCode; //КОД
    @JsonProperty("funds_sender_code")
    private String fundsSenderCode; //КБЕ
    @JsonProperty("transaction_reason")
    private String transactionReason; //Цель перевода
    @JsonProperty("postal_code")
    private String postalCode; //Индекс
//    private String country; //Страна
    @JsonProperty("region")
    private String region; //Область/Регион
    @JsonProperty("district")
    private String district; //Населенный пункт
    @JsonProperty("addr_line1")
    private String addressLine1; //Полный Адрес
    @JsonProperty("contact_phone")
    private String emailAddress; //Адрес электронной почты
    @JsonProperty("national_number")
    private String nationalNumber; //Номер телефона
}
