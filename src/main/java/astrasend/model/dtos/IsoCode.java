package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IsoCode implements Serializable {
    private String countryCode;
    private String currencyCode;
}
