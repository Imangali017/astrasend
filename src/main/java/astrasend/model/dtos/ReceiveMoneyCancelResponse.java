package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneyCancelResponse extends TransactionBase implements Serializable {
    private ForeignRemoteSystem foreignRemoteSystem;
    private String paidDate;
    private String paidTime;
    private String unpaidDate;
    private String unpaidTime;
    private String settlementDate;
    private int[] errorQueueCount;
}
