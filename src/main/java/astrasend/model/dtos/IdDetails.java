package astrasend.model.dtos;

import astrasend.model.enums.YesNo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IdDetails implements Serializable {
    private String idType;
    private String idCountryOfIssue;
    private String idPlaceOfIssue;
    private String idIssuerCode;
    private String idSeries;
    private String idNumberShort;
    private String idNumber;
    private String idIssueDate;
    private YesNo doesTheIdHaveAnExpirationDate;
    private String idExpirationDate;
}
