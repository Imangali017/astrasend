package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Customer implements Serializable {
    private GeneralName name;
    private PreferredCustomer preferredCustomer;
    private ComplianceDetails complianceDetails;
    private String email;
    private String contactPhone;
    private String dateOfBirth;
    private MobilePhone mobilePhone;
    private String profileDigest;
    private BankDetails bankDetails;
    private WalletDetails walletDetails;
    private CreditDebitCardDetails creditDebitCardDetails;
    private ExternalProcessors externalProcessors;
}
