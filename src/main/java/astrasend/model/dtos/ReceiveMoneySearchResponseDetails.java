package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneySearchResponseDetails implements Serializable {
    private TransferDto transfer;
    private String mtcn;
    private Sender sender;
    private Receiver receiver;
    private String payStatusDescription;

}
