package astrasend.model.dtos;

import java.io.Serializable;
import java.math.BigInteger;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneyPayReply implements Serializable {
    private Financials financials;
    private String mtcn;
    private String newMtcn;
    private BigInteger newPointsEarned;
    private String paidDate;
    private String paidTime;
    private String settlementDate;
    private ForeignRemoteSystem foreignRemoteSystem;
}
