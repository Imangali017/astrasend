package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreditDebitCardDetails implements Serializable {
    private String cardNumber;
    private String messageText;
    private String encryptionKey;
    private String trackData;
    private String cvviiCode;
    private String expirationDate;
    private String authorizationCode;
    private String issuingBank;
    private String lastUsageDate;
    private String creditCardErrorFlags;
    private String cvv2VerifyCode;
    private String lastScoreValue;
    private String productCode;
    private String scoringResults;
    private String cardType;
    private String cardDigest;
    private ChannelGeneralName name;
    private PinDebitDetails pinDebit;
}
