package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExternalProcessors implements Serializable {
    private String token;
}
