package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentReceiverDto implements Serializable {
    private String senderIdNumber;
    private String payAmount;
    private String senderCountry;
    private String statusTransaction;
    private String senderFirstName;
    private String senderLastName;
    private String senderMiddleName;
    private LocalDate dateOfBirth;
    private String countryOfBirth;
    private String cityOfBirth;
    private String idType;
    private String seriesAndNumberDUL;
    private LocalDate idIssueDate;
    private String issuingAuthority;
    private String idCountryOfIssue;
    private String idExpirationDate;
    private Boolean doesTheIdHaveAnExpirationDate;
    private Boolean isAResident;
    private String iin;
    private String transactionReason;
    private String knp;
    private String kod;
    private String kbe;
    private String purposeOfTranslation;
    private String index;
    private String countryCode;
    private String region;
    private String paymentDestinationCode;
    private String postalCode;
    private String locality;
    private String email;
    private String phoneNumber;
    private String mtcn;
}
