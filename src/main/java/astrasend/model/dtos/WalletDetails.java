package astrasend.model.dtos;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WalletDetails implements Serializable {
    private String walletAccountNumber;
    private String serviceProviderName;
    private String serviceProviderCode;
    private String serviceProviderType;
}
