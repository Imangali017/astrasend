package astrasend.model.dtos;

import astrasend.model.enums.DeviceTypeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceType implements Serializable {
    private String id;
    private DeviceTypeType type;
}
