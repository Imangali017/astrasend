package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTransaction implements Serializable {
    private Sender sender;
    private Receiver receiver;
    private Financials financials;
    private PaymentDetails paymentDetails;
    private String filingDate;
    private String filingTime;
    private String paidDate;
    private String paidTime;
    private String payStatusDescription;
    private String mtcn;
    private String newMtcn;
    private String partnerMtcn;
}
