package astrasend.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveMoneySearchRequest extends TransactionBase implements Serializable {
    private ForeignRemoteSystem foreignRemoteSystem;
    private PaymentTransaction paymentTransaction;
    private String searchFlag;
}
