package astrasend.model.dtos;

import astrasend.model.enums.ChannelType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Channel implements Serializable {
    private ChannelType type;
    private String name;
    private String version;
}
