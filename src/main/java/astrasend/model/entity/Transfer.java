package astrasend.model.entity;

import astrasend.model.enums.FinalStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "transfer")
public class Transfer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName; //Фамилия отправителя
    @Column(name = "last_name")
    private String lastName; //Имя отправителя
    @Column(name = "middle_name")
    private String middleName; //Отчество (при наличии) отправителя
    @Column(name = "date_of_birth")
    private String dateOfBirth; //Дата рождения отправителя
    @Column(name = "sender_country")
    private String senderCountry; //Страна
    @Column(name = "postal_code")
    private String postalCode; //Индекс
    @Column(name = "region")
    private String region; //Область/Регион
    @Column(name = "district")
    private String district; //Населенный пункт
    @Column(name = "addr_line1")
    private String addressLine1; //Полный Адрес
    @Column(name = "paid_date")
    private LocalDateTime paidDate; //Дата и время отправки
    @Column(name = "originating_city")
    private String originatingCity; //Пункт отправки
    @Column(name = "destination_city")
    private String destinationCity; //Пункт получателя
    @Column(name = "exchange_rate")
    private String exchangeRate; //Курс обмена валют
    @Column(name = "origin_country")
    private String originCountry; //Страна отправки
    @Column(name = "origin_currency_code")
    private String originCurrencyCode; //Валюта отправки
    @Column(name = "destination_country")
    private String destinationCountry; //Страна получателя
    @Column(name = "destination_currency_code")
    private String destinationCurrencyCode; //Валюта получателя
    @Column(name = "id_type")
    private String idType; //Тип ДУЛ Вручную сейчас Кассиры выбирают из справочника
    @Column(name = "id_series")
    private String idSeries; //Серия и номер ДУЛ
    @Column(name = "id_issue_date")
    private String idIssueDate; //Дата выдачи ДУЛ
    @Column(name = "id_country_of_issue")
    private String idCountryOfIssue; //Орган, выдавший ДУЛ
    @Column(name = "id_place_of_issue")
    private String idPlaceOfIssue; //Страна выдачи ДУЛ
    @Column(name = "id_expiration_date")
    private String idExpirationDate; //Дата окончания срока действия ДУЛ
    @Column(name = "is_a_resident")
    private String isAResident; //Является ли клиент резидентом
    @Column(name = "tax_payers_registration_number")
    private String taxPayersRegistrationNumber; //ИИН
    @Column(name = "other_purpose_of_transaction")
    private String otherPurposeOfTransaction; //Назначение перевода
    @Column(name = "beneficiary_code")
    private String beneficiaryCode; //КНП
    @Column(name = "sender_residency_code")
    private String senderResidencyCode; //КОД
    @Column(name = "funds_sender_code")
    private String fundsSenderCode; //КБЕ
    @Column(name = "transaction_reason")
    private String transactionReason; //Цель перевода
    @Column(name = "email_address")
    private String emailAddress; //Адрес электронной почты
    @Column(name = "national_number")
    private String nationalNumber; //Номер телефона
    @Column(name = "pay_status_description")
    private String payStatusDescription;
    @Column(name = "mtcn")
    private String mtcn;
    @Column(name = "new_mtcn")
    private String newMtcn;
    @Column(name = "q_p_status")
    private String qPStatus;
    @Column(name = "final_status")
    private FinalStatus finalStatus;
    @Column(name = "number_transaction_qp")
    private String numberTransactionQP;
    @Column(name = "pay_amount")
    private BigDecimal payAmount;
    @Column(name = "receiver_first_name")
    private String receiverFirstName;
    @Column(name = "receiver_last_name")
    private String receiverLastName;
    @Column(name = "receiver_middle_name")
    private String receiverMiddleName;
    private String operationId;
    private String departmentId;
    private String referenceNumber;

}
