package astrasend.model.enums;

public enum StatusType {
    WILL,
    PAY
}
