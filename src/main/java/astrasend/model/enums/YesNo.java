package astrasend.model.enums;

public enum YesNo {
    Y,
    N;
}
