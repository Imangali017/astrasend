package astrasend.model.enums;

public enum DeviceTypeType {
    ATM,
    WEB,
    RETAIL,
    MOBILE,
    CSC,
    SST;

    public String value() {
        return name();
    }

    public static DeviceTypeType fromValue(String v) {
        return valueOf(v);
    }
}
