package astrasend.model.enums;

public enum ChannelType {
    H2H("H2H"),
    CSC("CSC"),
    ASTRAPOS("ASTRAPOS");

    private final String value;

    ChannelType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ChannelType fromValue(String value) {
        for (ChannelType type : ChannelType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid channel type: " + value);
    }
}
