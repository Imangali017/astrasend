package astrasend.model.enums;

public enum PaymentType {
    CREDIT_CARD("CreditCard"),
    DEBIT_CARD("DebitCard"),
    CASH("Cash"),
    ADJUSTMENT("Adjustment"),
    REINSTATE("Reinstate"),
    REFUND("Refund"),
    CANCEL("Cancel"),
    CANCEL_FUNDED("CancelFunded"),
    BANK_ONLINE("BankOnline"),
    BANK_OFFLINE("BankOffline"),
    BANK_ACCOUNT("BankAccount"),
    WALLET("Wallet");

    private final String value;

    PaymentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static PaymentType fromValue(String value) {
        for (PaymentType paymentType : PaymentType.values()) {
            if (paymentType.value.equals(value)) {
                return paymentType;
            }
        }
        throw new IllegalArgumentException("Invalid PaymentType value: " + value);
    }
}
