package astrasend.model.enums;

public enum NameSuffix{
    JR("Jr."),
    SR("Sr."),
    I("I"),
    II("II"),
    III("III"),
    ESQ("Esq.");

    private final String value;

    NameSuffix(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NameSuffix fromValue(String v) {
        for (NameSuffix c : NameSuffix.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
