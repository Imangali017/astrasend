package astrasend.model.enums;

public enum CurrencyType {
    EUR,
    KZT,
    RUB
}
