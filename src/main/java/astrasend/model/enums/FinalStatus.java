package astrasend.model.enums;

public enum FinalStatus {
    PAID("Выплачен"),
    PAID_TO_ASTRASEND("Выплачен в Астрасенд"),
    AVAILABLE_FOR_PAYMENT("Доступен к выплате"),
    NEED_TO_CANCEL("Необходимо отменить");

    private final String value;

    FinalStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static FinalStatus fromValue(String value) {
        for (FinalStatus type : FinalStatus.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid channel type: " + value);
    }

}
