package astrasend.model.enums;

public enum NamePrefix {
    MR("Mr."),
    MS("Ms."),
    MRS("Mrs."),
    MISS("Miss"),
    DR("Dr."),
    PROFESSOR("Professor"),
    SIR("Sir"),
    MADAM("Madam"),
    M("M"),
    S("S"),
    F("F");

    private final String value;

    NamePrefix(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NamePrefix fromValue(String v) {
        for (NamePrefix c : NamePrefix.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
