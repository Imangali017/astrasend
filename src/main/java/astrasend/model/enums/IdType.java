package astrasend.model.enums;

public enum IdType {
    A("Паспорт гражданина Казахстана"),
    B("Национальное удостоверение личности гражданина Казахстана"),
    E("Другое");


    public final String value;

    private IdType(String value) {
        this.value = value;
    }
}
