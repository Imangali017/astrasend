package astrasend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AstrasendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AstrasendApplication.class, args);
	}

}
