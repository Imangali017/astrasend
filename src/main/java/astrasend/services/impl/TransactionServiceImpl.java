package astrasend.services.impl;

import astrasend.clients.EsbClient;
import astrasend.clients.QPClient;
import astrasend.exceptions.AstrasendInCorrectResponseException;
import astrasend.exceptions.CustomErrorException;
import astrasend.exceptions.TransferNotFoundException;
import astrasend.mapper.ReceiveSearchMapper;
import astrasend.mapper.TransferMapper;
import astrasend.model.dtos.Address;
import astrasend.model.dtos.ComplianceDetails;
import astrasend.model.dtos.CountryCurrencyInfo;
import astrasend.model.dtos.EsbResponse;
import astrasend.model.dtos.Financials;
import astrasend.model.dtos.ForeignRemoteSystem;
import astrasend.model.dtos.GeneralName;
import astrasend.model.dtos.IdDetails;
import astrasend.model.dtos.InternationalPhoneNumber;
import astrasend.model.dtos.IsoCode;
import astrasend.model.dtos.MobilePhone;
import astrasend.model.dtos.PaymentDetails;
import astrasend.model.dtos.PaymentTransaction;
import astrasend.model.dtos.ReceiveMoneyCancelRequest;
import astrasend.model.dtos.ReceiveMoneyCancelResponse;
import astrasend.model.dtos.ReceiveMoneyPayReply;
import astrasend.model.dtos.ReceiveMoneyPayRequest;
import astrasend.model.dtos.ReceiveMoneySearchReply;
import astrasend.model.dtos.ReceiveMoneySearchRequest;
import astrasend.model.dtos.ReceiveMoneySearchResponse;
import astrasend.model.dtos.ReceiveMoneyStatusRequest;
import astrasend.model.dtos.Receiver;
import astrasend.model.dtos.ReportStatusDto;
import astrasend.model.dtos.Sender;
import astrasend.model.dtos.TransferDto;
import astrasend.model.dtos.abis.AbisMoneyPayRequest;
import astrasend.model.dtos.abis.AbisMoneyPayResponse;
import astrasend.model.entity.Transfer;
import astrasend.model.enums.Code;
import astrasend.model.enums.FinalStatus;
import astrasend.model.enums.NameType;
import astrasend.model.enums.YesNo;
import astrasend.repositories.TransferRepository;
import astrasend.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    private final EsbClient esbClient;
    private final ReceiveSearchMapper receiveSearchMapper;
    private final TransferRepository transferRepository;
    private final TransferMapper transferMapper;
    private final QPClient qpClient;
    private static final String SUCCESS_STATUS = "ФСА";
    private static final String SUCCESS_STATUS_CANCEL = "A";
    @Value("${astrasend.identifier}")
    private String identifier;
    @Value("${astrasend.location-id}")
    private String locationId;
    @Value("${astrasend.counter-id}")
    private String counterId;
    @Value("${astrasend.template-id}")
    private String templateId;

    @Autowired
    public TransactionServiceImpl(EsbClient esbClient,
                                  ReceiveSearchMapper receiveSearchMapper,
                                  TransferRepository transferRepository,
                                  TransferMapper transferMapper,
                                  QPClient qpClient) {
        this.esbClient = esbClient;
        this.receiveSearchMapper = receiveSearchMapper;
        this.transferRepository = transferRepository;
        this.transferMapper = transferMapper;
        this.qpClient = qpClient;
    }

    @Override
    public byte[] generateReport(String departmentId, LocalDate startDate, LocalDate endDate) {
        String partnerName = "BEREKE BANK";
        String branchAddress = "g Almaty, pr-kt Al-Farabi, d 13/1";
        List<TransferDto> transferDtoList = findAllByDepartmentIdIssueDateBetween(departmentId, startDate, endDate).getTransferDtoList();
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Отчет по операциям в Системе Астрасенд");

            // Установка ширины столбцов
            sheet.setColumnWidth(0, 6000); // Дата выгрузки отчета
            sheet.setColumnWidth(1, 10000); // Название партнера
            sheet.setColumnWidth(2, 15000); // Адрес отделения
            sheet.setColumnWidth(3, 10000); // Период формирования отчета

            // Заголовок
            Row titleRow = sheet.createRow(0);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("Отчет по операциям в Системе Астрасенд");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3)); // Объединяем ячейки для заголовка

            // Данные
            Row headerRow = sheet.createRow(1);
            String[] headers = {"Дата выгрузки отчета", "Название партнера", "Адрес отделения", "Период формирования отчета"};
            for (int i = 0; i < headers.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(headers[i]);
            }
            // Заполнение информации о партнере и адресе отделения
            Row infoRow = sheet.createRow(2);
            infoRow.createCell(0).setCellValue(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
            infoRow.createCell(1).setCellValue(partnerName);
            infoRow.createCell(2).setCellValue(branchAddress);
            infoRow.createCell(3).setCellValue(startDate + " - " + endDate);
            // Добавление заголовков для данных о переводах
            Row dataHeaderRow = sheet.createRow(3);
            String[] dataHeaders = {"КНДП", "Дата операции", "Время операции", "Тип операции", "ФИО отправителя",
                    "Страна отправки", "Валюта отправки", "Сумма отправки", "Плата за отправку перевода",
                    "Скидка по промо-коду", "Общая сумма при отправке", "ФИО получателя", "Страна выплаты",
                    "Валюта выплаты", "Сумма выплаты", "Курс пересчета валют", "Тип перевода",
                    "Вознаграждение Партнера", "Код оператора", "Отделение"};
            for (int i = 0; i < dataHeaders.length; i++) {
                Cell cell = dataHeaderRow.createCell(i);
                cell.setCellValue(dataHeaders[i]);
            }

            sheet.setColumnWidth(4, 8000); // ФИО отправителя
            sheet.setColumnWidth(5, 6000); // Страна отправки
            sheet.setColumnWidth(6, 6000); // Валюта отправки
            sheet.setColumnWidth(7, 6000); // Сумма отправки
            sheet.setColumnWidth(8, 8000); // Плата за отправку перевода
            sheet.setColumnWidth(9, 6000); // Скидка по промо-коду
            sheet.setColumnWidth(10, 8000); // Общая сумма при отправке
            sheet.setColumnWidth(11, 8000); // ФИО получателя
            sheet.setColumnWidth(12, 6000); // Страна выплаты
            sheet.setColumnWidth(13, 6000); // Валюта выплаты
            sheet.setColumnWidth(14, 6000); // Сумма выплаты
            sheet.setColumnWidth(15, 6000); // Курс пересчета валют
            sheet.setColumnWidth(16, 6000); // Тип перевода
            sheet.setColumnWidth(17, 8000); // Вознаграждение Партнера
            sheet.setColumnWidth(18, 6000); // Код оператора
            sheet.setColumnWidth(19, 6000); // Отделение
            // Заполнение данных о переводах
            int rowNum = 4; // Начало данных о переводах со строки 4
            for (TransferDto transferDto : transferDtoList) {
                Row dataRow = sheet.createRow(rowNum++);

                // Добавьте остальные данные о переводе согласно вашим требованиям
            }
            // Преобразование в массив байтов
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ReportStatusDto findAllByOperationIdIssueDateBetween(String operationId, LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.atStartOfDay().plusDays(1).minusSeconds(1);
        List<Transfer> allByIdIssueDateBetween = transferRepository.findAllByOperationIdAndPaidDateBetween(operationId, startDateTime, endDateTime);
        return buildResponse(allByIdIssueDateBetween);
    }

    private ReportStatusDto buildResponse(List<Transfer> allByIdIssueDateBetween) {
        List<TransferDto> transferDtos = transferMapper.toDto(allByIdIssueDateBetween);
        ReportStatusDto reportStatusDto = new ReportStatusDto();
        reportStatusDto.setTransferDtoList(transferDtos);
        reportStatusDto.setTransferAmount(allByIdIssueDateBetween.size());
        BigDecimal sum = transferDtos.stream().map(TransferDto::getPayAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        reportStatusDto.setTransferSum(sum);
        return reportStatusDto;
    }

    @Override
    public ReportStatusDto findAllByDepartmentIdIssueDateBetween(String departmentId, LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.atStartOfDay().plusDays(1).minusSeconds(1);
        List<Transfer> allByIdIssueDateBetween = transferRepository.findAllByDepartmentIdAndPaidDateBetween(departmentId, startDateTime, endDateTime);
        return buildResponse(allByIdIssueDateBetween);
    }

    @Override
    public ReceiveMoneySearchResponse receiveMoneyStatus(String mtcn, String operatorId) {
        Transfer transfer = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        TransferDto transferDto = transferMapper.toDto(transfer);
        var request = formRequestForReceiveMoneyStatus(transferDto, operatorId);
        var response = esbClient.doReceivePayStatusInquiry(request);
        throwCustomErrorException(response.toString());
        return builderResponseMoneyPayStatus(response, mtcn);
    }

    private ReceiveMoneySearchResponse builderResponseMoneyPayStatus(EsbResponse<ReceiveMoneySearchReply> response, String mtcn) {
        ReceiveMoneySearchResponse receiveMoneySearchResponse = new ReceiveMoneySearchResponse();
        String statusDescription = response.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0).getPayStatusDescription();
        Receiver receiver = response.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0).getReceiver();
        Sender sender = response.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0).getSender();
        receiveMoneySearchResponse.setReceiver(receiver);
        receiveMoneySearchResponse.setSender(sender);
        receiveMoneySearchResponse.setMtcn(mtcn);
        Transfer transfer = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        EsbResponse<AbisMoneyPayResponse> abisPayResponse = qpClient.doReceiveMoneySearch(formAbisMoneyCancelAndStatusRequest(mtcn));
        String abisStatus = abisPayResponse.getRsParms().getStatus();
        if (abisStatus.equals(SUCCESS_STATUS) && statusDescription.equals("PAID")) {
            transfer.setFinalStatus(FinalStatus.PAID);
        }
        if (! abisStatus.equals(SUCCESS_STATUS) && statusDescription.equals("PAID")) {
            transfer.setFinalStatus(FinalStatus.PAID_TO_ASTRASEND);
        }
        if (! abisStatus.equals(SUCCESS_STATUS) && statusDescription.equals("WILL CALL")) {
            transfer.setFinalStatus(FinalStatus.AVAILABLE_FOR_PAYMENT);
        }
        if (abisStatus.equals(SUCCESS_STATUS) && statusDescription.equals("CAN")) {
            transfer.setFinalStatus(FinalStatus.NEED_TO_CANCEL);
        }
        transferRepository.save(transfer);
        receiveMoneySearchResponse.setFinalStatus(transfer.getFinalStatus().getValue());
        return receiveMoneySearchResponse;
    }

    @Override
    public ReceiveMoneySearchResponse receiveMoneySearch(String mtcn, String operatorId, String departmentId) {
        var request = formRequestForReceiveMoneySearch(mtcn, operatorId);
        var response = esbClient.doReceiveMoneySearch(request);
        throwCustomErrorException(response.toString());
        return builderResponseMoneyPaySearch(response, mtcn, operatorId, departmentId);
    }

    private void throwCustomErrorException(String status) {
        Pattern pattern = Pattern.compile("code=([A-Z0-9]+).*?message=([^,}]+)");
        Matcher matcher = pattern.matcher(status);
        if (matcher.find()) {
            String code = matcher.group(1);
            String message = matcher.group(2);
            throw new CustomErrorException(code, message);
        }
    }

    @Override
    public ReceiveMoneySearchResponse cancelMoneySearch(String mtcn, String operatorId) {
        var request = formRequestForReceiveMoneySearch(mtcn, operatorId);
        request.setSearchFlag("CANCEL_PAID");
        var response = esbClient.doCancelMoneySearch(request);
        throwCustomErrorException(response.toString());
        return builderResponseMoneyCancel(response, mtcn, null);
    }

    @Override
    public void receiveMoneyPay(TransferDto requestDto) {
        Transfer transfer = transferRepository.findByMtcn(requestDto.getMtcn()).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + requestDto.getMtcn()));
        var request = formRequestForReceiveMoneyPay(requestDto);
        EsbResponse<ReceiveMoneyPayReply> receiveMoneyPayResponseEsbResponse = esbClient.doReceiveMoneyPay(request);
        throwCustomErrorException(receiveMoneyPayResponseEsbResponse.toString());
        EsbResponse<AbisMoneyPayResponse> abisPayResponse = qpClient.doReceiveMoneySearch(formAbisMoneyPayRequest(requestDto));
        if (abisPayResponse != null && receiveMoneyPayResponseEsbResponse != null) {
            EsbResponse<ReceiveMoneySearchReply> statusInquiry = esbClient.doReceivePayStatusInquiry(formRequestForReceiveMoneyStatus(requestDto, requestDto.getOperationId()));
            throwCustomErrorException(statusInquiry.toString());
            String abisStatus = abisPayResponse.getRsParms().getStatus();
            transfer.setQPStatus(abisStatus);
            transfer.setNumberTransactionQP(abisPayResponse.getRsParms().getOp() != null ? abisPayResponse.getRsParms().getOp() : abisPayResponse.getRsParms().getOperation());
            String payStatusDescription = statusInquiry.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0).getPayStatusDescription();
            if (abisStatus.equals(SUCCESS_STATUS) && payStatusDescription.equals("PAID")) {
                transfer.setFinalStatus(FinalStatus.PAID);
            }
            if (! abisStatus.equals(SUCCESS_STATUS) && payStatusDescription.equals("PAID")) {
                transfer.setFinalStatus(FinalStatus.PAID_TO_ASTRASEND);
            }
            if (! abisStatus.equals(SUCCESS_STATUS) && payStatusDescription.equals("WILL CALL")) {
                transfer.setFinalStatus(FinalStatus.AVAILABLE_FOR_PAYMENT);
            }
            if (abisStatus.equals(SUCCESS_STATUS) && payStatusDescription.equals("CAN")) {
                transfer.setFinalStatus(FinalStatus.NEED_TO_CANCEL);
            }
            transferRepository.save(transfer);
        }
    }

    @Override
    public void cancelMoneyPay(String mtcn, String operatorId) {
        var request = formRequestForReceiveMoneyCancel(mtcn, operatorId);
        EsbResponse<ReceiveMoneyCancelResponse> response = esbClient.doCancelMoneyPay(request);
        throwCustomErrorException(response.toString());
        EsbResponse<AbisMoneyPayResponse> abisPayResponse = qpClient.doReceiveMoneySearch(formAbisMoneyCancelAndStatusRequest(mtcn));
        Transfer transfer = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        if (response != null && abisPayResponse != null && abisPayResponse.getRsParms().getStatus().equals("ФСА")) {
            transfer.setFinalStatus(FinalStatus.NEED_TO_CANCEL);
            transferRepository.save(transfer);
        } else {
            throw new RuntimeException("Отмена не прошло");
        }
    }

    @Override
    public TransferDto searchDetails(String mtcn) {
        Transfer transfer = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        return transferMapper.toDto(transfer);
    }

    private ReceiveMoneySearchResponse builderResponseMoneyPaySearch(EsbResponse<ReceiveMoneySearchReply> response,
                                                                     String mtcn, String operatorId, String departmentId) {
        PaymentTransaction paymentTransaction = response.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0);
        if (paymentTransaction == null) {
            throw new AstrasendInCorrectResponseException(Code.INCORRECT_RESPONSE, "Ошибка при обработке ответа");
        }
        var transfer = receiveSearchMapper.toEntity(paymentTransaction);
        transfer.setPaidDate(convertToDateTime(paymentTransaction.getFilingDate(), paymentTransaction.getFilingTime()));
        transfer.setPayAmount(BigDecimal.valueOf(paymentTransaction.getFinancials().getPayAmount() / 100));
        transfer.setOperationId(operatorId);
        transfer.setDepartmentId(departmentId);
        transfer.setReferenceNumber(response.getRsParms().getForeignRemoteSystem().getReferenceNo());
        Optional<Transfer> byMtcn = transferRepository.findByMtcn(mtcn);
        Transfer savedTransfer = byMtcn.orElseGet(() -> transferRepository.save(transfer));
        savedTransfer.setReferenceNumber(response.getRsParms().getForeignRemoteSystem().getReferenceNo());
        transferRepository.save(savedTransfer);
        TransferDto dto = transferMapper.toDto(savedTransfer);
        ReceiveMoneySearchResponse receiveMoneySearchResponse = new ReceiveMoneySearchResponse();
        receiveMoneySearchResponse.setReceiver(getReceiver(dto));
        receiveMoneySearchResponse.setSender(getSender(dto));
        receiveMoneySearchResponse.setPayAmount(transfer.getPayAmount());
        receiveMoneySearchResponse.setCurrency(transfer.getOriginCurrencyCode());
        receiveMoneySearchResponse.setMtcn(dto.getMtcn());
        receiveMoneySearchResponse.setSenderCountry(dto.getSenderCountry());
        receiveMoneySearchResponse.setFinalStatus(dto.getFinalStatus() != null ? dto.getFinalStatus().getValue() : null);
        return receiveMoneySearchResponse;
    }

    private LocalDateTime convertToDateTime(String filingDate, String filingTime) {
        if (filingDate == null || filingTime == null) {
            return null;
        }
        String[] dateParts = filingDate.split("(?<=\\G.{2})");
        String[] timeParts = filingTime.split(":");
        int year = Integer.parseInt(dateParts[2]);
        int month = Integer.parseInt(dateParts[1]);
        int day = Integer.parseInt(dateParts[0]);
        int hour = Integer.parseInt(timeParts[0]);
        int minute = Integer.parseInt(timeParts[1]);
        int second = Integer.parseInt(timeParts[2]);
        return LocalDateTime.of(year, month, day, hour, minute, second);
    }

    private AbisMoneyPayRequest formAbisMoneyCancelAndStatusRequest(String mtcn) {
        AbisMoneyPayRequest abisMoneyPayRequest = new AbisMoneyPayRequest();
        abisMoneyPayRequest.setKndp(mtcn);
        return abisMoneyPayRequest;
    }

    private AbisMoneyPayRequest formAbisMoneyPayRequest(TransferDto dto) {
        AbisMoneyPayRequest abisMoneyPayRequest = new AbisMoneyPayRequest();
        abisMoneyPayRequest.setKndp(dto.getMtcn());
        abisMoneyPayRequest.setSum(dto.getPayAmount());
        abisMoneyPayRequest.setCurrency(dto.getOriginCurrencyCode());
        abisMoneyPayRequest.setSenderName(dto.getFirstName());
        abisMoneyPayRequest.setSenderCountry(dto.getSenderCountry());
        abisMoneyPayRequest.setReceiverName(dto.getReceiverFirstName());
        abisMoneyPayRequest.setDocument(dto.getIdType());
        abisMoneyPayRequest.setDocNum(dto.getIdSeries());
        abisMoneyPayRequest.setDocIssue(dto.getIdCountryOfIssue() + "," + dto.getIdExpirationDate());
        abisMoneyPayRequest.setReceiverInn(dto.getTaxPayersRegistrationNumber());
        abisMoneyPayRequest.setDetails("Выплата перевода Астрасенд");
        abisMoneyPayRequest.setKbe(dto.getFundsSenderCode());
        abisMoneyPayRequest.setKod(dto.getSenderResidencyCode());
        abisMoneyPayRequest.setKnp(dto.getBeneficiaryCode());
        abisMoneyPayRequest.setUserId("u16661");
        return abisMoneyPayRequest;
    }

    private ReceiveMoneySearchResponse builderResponseMoneyCancel(EsbResponse<ReceiveMoneySearchReply> response, String mtcn, AbisMoneyPayResponse abisPayResponse) {
        var transfer = receiveSearchMapper.toEntity(response.getRsParms().getPaymentTransactions().getPaymentTransaction().get(0));
        Transfer transferFromDb = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        if (abisPayResponse != null) {
            transferFromDb.setQPStatus(abisPayResponse.getStatus());
            transferFromDb.setNumberTransactionQP(abisPayResponse.getOp() != null ? abisPayResponse.getOp() : abisPayResponse.getOperation());
            String abisStatus = abisPayResponse.getStatus();
            if (! abisStatus.equals(SUCCESS_STATUS_CANCEL) || ! transfer.getPayStatusDescription().equals("PAID") || transferFromDb.getFinalStatus() != FinalStatus.PAID) {
                throw new RuntimeException("Статус оплаты не соответствует ожидаемому");
            }
        }
        Transfer savedTransfer = transferRepository.save(transferFromDb);
        TransferDto dto = transferMapper.toDto(savedTransfer);
        ReceiveMoneySearchResponse receiveMoneySearchResponse = new ReceiveMoneySearchResponse();
        receiveMoneySearchResponse.setReceiver(getReceiver(dto));
        receiveMoneySearchResponse.setSender(getSender(dto));
        receiveMoneySearchResponse.setPayAmount(dto.getPayAmount());
        receiveMoneySearchResponse.setMtcn(dto.getMtcn());
        receiveMoneySearchResponse.setSenderCountry(dto.getSenderCountry());
        receiveMoneySearchResponse.setFinalStatus(dto.getFinalStatus().getValue());
        return receiveMoneySearchResponse;
    }

    public Receiver getReceiver(TransferDto dto) {
        Receiver receiver = new Receiver();
        GeneralName receiverGeneralName = new GeneralName();
        receiverGeneralName.setFirstName(dto.getReceiverFirstName());
        receiverGeneralName.setMiddleName(dto.getReceiverMiddleName());
        receiverGeneralName.setLastName(dto.getReceiverLastName());
        receiver.setName(receiverGeneralName);
        return receiver;
    }

    public Sender getSender(TransferDto dto) {
        Sender sender = new Sender();
        GeneralName senderGeneralName = new GeneralName();
        senderGeneralName.setFirstName(dto.getFirstName());
        senderGeneralName.setMiddleName(dto.getMiddleName());
        senderGeneralName.setLastName(dto.getLastName());
        sender.setName(senderGeneralName);
        return sender;
    }

    private ReceiveMoneySearchRequest formRequestForReceiveMoneySearch(String mtcn, String operatorId) {
        ReceiveMoneySearchRequest request = new ReceiveMoneySearchRequest();
        ForeignRemoteSystem foreignRemoteSystem = new ForeignRemoteSystem();
        foreignRemoteSystem.setIdentifier(identifier);
        Optional<Transfer> transfer = transferRepository.findByMtcn(mtcn);
        if (transfer.isPresent()) {
            foreignRemoteSystem.setReferenceNo(transfer.get().getReferenceNumber());
        } else {
            foreignRemoteSystem.setReferenceNo(RandomStringUtils.randomAlphanumeric(16));
        }
        foreignRemoteSystem.setLocationId(locationId);
        foreignRemoteSystem.setCounterId(counterId);
        foreignRemoteSystem.setOperatorId(operatorId);
        request.setForeignRemoteSystem(foreignRemoteSystem);
        PaymentTransaction payment = new PaymentTransaction();
        PaymentDetails paymentDetails = new PaymentDetails();
        CountryCurrencyInfo destinationCountryCurrency = new CountryCurrencyInfo();
        destinationCountryCurrency.setCountryName("KAZ");
        IsoCode isoCode = new IsoCode();
        isoCode.setCountryCode("KAZ");
        isoCode.setCurrencyCode("RUB");
        destinationCountryCurrency.setIsoCode(isoCode);
        paymentDetails.setDestinationCountryCurrency(destinationCountryCurrency);
        payment.setPaymentDetails(paymentDetails);
        payment.setMtcn(mtcn);
        request.setPaymentTransaction(payment);
        return request;
    }

    private ReceiveMoneyStatusRequest formRequestForReceiveMoneyStatus(TransferDto transferDto, String operatorId) {
        ReceiveMoneyStatusRequest request = new ReceiveMoneyStatusRequest();
        ForeignRemoteSystem foreignRemoteSystem = new ForeignRemoteSystem();
        foreignRemoteSystem.setIdentifier(identifier);
        foreignRemoteSystem.setReferenceNo(transferDto.getReferenceNumber());
        foreignRemoteSystem.setLocationId(locationId);
        foreignRemoteSystem.setCounterId(counterId);
        foreignRemoteSystem.setOperatorId(operatorId);
        request.setForeignRemoteSystem(foreignRemoteSystem);
        PaymentDetails paymentDetails = new PaymentDetails();
        CountryCurrencyInfo destinationCountryCurrency = new CountryCurrencyInfo();
        destinationCountryCurrency.setCountryName("KAZ");
        IsoCode isoCode = new IsoCode();
        isoCode.setCountryCode("KAZ");
        isoCode.setCurrencyCode("RUB");
        destinationCountryCurrency.setIsoCode(isoCode);
        paymentDetails.setDestinationCountryCurrency(destinationCountryCurrency);
        request.setPaymentDetails(paymentDetails);
        request.setMtcn(transferDto.getMtcn());
        return request;
    }

    private ReceiveMoneyCancelRequest formRequestForReceiveMoneyCancel(String mtcn, String operatorId) {
        ReceiveMoneyCancelRequest request = new ReceiveMoneyCancelRequest();
        ForeignRemoteSystem foreignRemoteSystem = new ForeignRemoteSystem();
        foreignRemoteSystem.setIdentifier(identifier);
        Transfer transfer = transferRepository.findByMtcn(mtcn).orElseThrow(() -> new TransferNotFoundException("Transfer not found for MTCN: " + mtcn));
        foreignRemoteSystem.setReferenceNo(transfer.getReferenceNumber());
        foreignRemoteSystem.setLocationId(locationId);
        foreignRemoteSystem.setCounterId(counterId);
        foreignRemoteSystem.setOperatorId(operatorId);
        request.setForeignRemoteSystem(foreignRemoteSystem);
        request.setMtcn(mtcn);
        return request;
    }

    private ReceiveMoneyPayRequest formRequestForReceiveMoneyPay(TransferDto dto) {
        var request = new ReceiveMoneyPayRequest();
        var receiver = new Receiver();
        var name = new GeneralName();
        name.setNameType(NameType.D);
        name.setFirstName(dto.getFirstName());
        name.setLastName(dto.getLastName());
        name.setMiddleName(dto.getMiddleName());
        receiver.setName(name);
        var complianceDetails = new ComplianceDetails();
        complianceDetails.setTemplateId(templateId); // "KAZ_01_P";
        var idDetails = new IdDetails();
        idDetails.setIdType(dto.getIdType()); // "E";
        idDetails.setIdCountryOfIssue(dto.getIdCountryOfIssue()); // "KAZ";
        idDetails.setIdNumber(dto.getIdSeries()); // "4617500451";
        idDetails.setIdIssueDate(dto.getIdIssueDate()); // "12032002";
        idDetails.setIdExpirationDate(dto.getIdExpirationDate());//""
        idDetails.setDoesTheIdHaveAnExpirationDate(dto.getDoesTheIdHaveAnExpirationDate().equals(true) ? YesNo.Y : YesNo.N); // YesNo.N;
        complianceDetails.setIdDetails(idDetails);
        complianceDetails.setDateOfBirth(dto.getDateOfBirth());//"07101992"
        var currentAddress = new Address();
        currentAddress.setAddrLine1(dto.getAddressLine1());//"JDNVUIHFDUGH 1 8 5"
        currentAddress.setAddrLine2(dto.getAddressLine2());//""
        currentAddress.setCity(dto.getDistrict());//"DSGFDDFG"
        currentAddress.setPostalCode(dto.getPostalCode());//""
        currentAddress.setCountry(dto.getIdCountryOfIssue());//"KAZ"
        currentAddress.setRegion(dto.getRegion());//""
        complianceDetails.setCurrentAddress(currentAddress);
        complianceDetails.setTransactionReason(dto.getTransactionReason());//"Medical treatment"
        complianceDetails.setCountryOfBirth(dto.getCountryOfBirth());//"SUN"
        String isAResidentValue = dto.getIsAResident();
        YesNo isAResidentEnum = isAResidentValue != null ? YesNo.valueOf(isAResidentValue) : null;
        complianceDetails.setIsAResident(isAResidentEnum);//YesNo.Y
        complianceDetails.setPersonalIdentificationNumber(dto.getPersonalIdentificationNumber());//"651127435960"
        complianceDetails.setClassifierOfThePurposeOfMtNumber(dto.getClassifierOfThePurposeOfMtNumber());//"123"
        complianceDetails.setAckFlag(dto.getAckFlag());//"x"
        complianceDetails.setFundsSenderCode(dto.getFundsSenderCode());//"12"
        complianceDetails.setBeneficiaryCode(dto.getBeneficiaryCode());//"81"
        complianceDetails.setOtherPurposeOfTransaction(dto.getOtherPurposeOfTransaction());//""
        Boolean marketingFlag = dto.getMarketingFlag();
        YesNo marketing = marketingFlag != null && marketingFlag ? YesNo.Y : YesNo.N;
        complianceDetails.setMarketingFlag(marketing);//YesNo.Y
        receiver.setComplianceDetails(complianceDetails);
        receiver.setEmail(dto.getEmailAddress());//""
        receiver.setContactPhone(dto.getNationalNumber());//"74862495991"
        var mobilePhone = new MobilePhone();
        var phoneNumber = new InternationalPhoneNumber();
        phoneNumber.setCountryCode(dto.getInternationalPhoneNumberCountry());//"7"
        phoneNumber.setNationalNumber(dto.getNationalNumber());//"4862495991"
        mobilePhone.setPhoneNumber(phoneNumber);
        receiver.setMobilePhone(mobilePhone);
        request.setReceiver(receiver);
        var paymentDetails = new PaymentDetails();
        var originatingCountryCurrency = new CountryCurrencyInfo();
        var originatingIsoCode = new IsoCode();
        originatingIsoCode.setCountryCode(dto.getOriginCountry());//"RUS"
        originatingIsoCode.setCurrencyCode(dto.getOriginCurrencyCode());//"RUB"
        originatingCountryCurrency.setIsoCode(originatingIsoCode);
        paymentDetails.setOriginatingCountryCurrency(originatingCountryCurrency);
        var destinationCountryCurrency = new CountryCurrencyInfo();
        var destinationIsoCode = new IsoCode();
        destinationIsoCode.setCountryCode("KAZ");
        destinationIsoCode.setCurrencyCode("RUB");
        destinationCountryCurrency.setIsoCode(destinationIsoCode);
        paymentDetails.setDestinationCountryCurrency(destinationCountryCurrency);
        paymentDetails.setOriginatingCity(dto.getOriginatingCity());//"Moscow"
        paymentDetails.setPaymentType(dto.getPaymentType());//PaymentType.CASH
        paymentDetails.setExchangeRate(Double.valueOf(dto.getExchangeRate()));//1.0
        var fixOnSend = dto.getFixOnSend();
        YesNo fixOnSendYesNo = fixOnSend != null && fixOnSend ? YesNo.Y : YesNo.N;
        paymentDetails.setFixOnSend(fixOnSendYesNo);//YesNo.N
        var originalDestinationCountryCurrency = new CountryCurrencyInfo();
        var originalIsoCode = new IsoCode();
        originalIsoCode.setCountryCode(dto.getOriginalDestinationCountryCurrencyIsoCode().getCountryCode());//"KAZ"
        originalIsoCode.setCurrencyCode(dto.getOriginalDestinationCountryCurrencyIsoCode().getCurrencyCode());//"RUB"
        originalDestinationCountryCurrency.setIsoCode(originalIsoCode);
        paymentDetails.setOriginalDestinationCountryCurrency(originalDestinationCountryCurrency);
        request.setPaymentDetails(paymentDetails);
        var financials = new Financials();
        financials.setOriginatorsPrincipalAmount(dto.getOriginatorsPrincipalAmount());//60000L
        financials.setDestinationPrincipalAmount(dto.getDestinationPrincipalAmount());//60000L
        financials.setPayAmount(Long.valueOf(dto.getPayAmount().toString()));//60000L
        request.setFinancials(financials);
        request.setMtcn(dto.getMtcn());//"2952440203"
        request.setNewMtcn(dto.getNewMtcn());//"160120242952440203"
        request.setPayOrDoNotPayIndicator(dto.getPayOrDoNotPayIndicator());//"P"
        var foreignRemoteSystem = new ForeignRemoteSystem();
        foreignRemoteSystem.setIdentifier(identifier);
        foreignRemoteSystem.setReferenceNo(dto.getReferenceNumber());
        foreignRemoteSystem.setLocationId(locationId);
        foreignRemoteSystem.setCounterId(counterId);
        foreignRemoteSystem.setOperatorId(dto.getOperationId());
        request.setForeignRemoteSystem(foreignRemoteSystem);
        return request;
    }

}
