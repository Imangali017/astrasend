package astrasend.services;

import astrasend.model.dtos.ReceiveMoneySearchResponse;
import astrasend.model.dtos.ReportStatusDto;
import astrasend.model.dtos.TransferDto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface TransactionService {

    byte[] generateReport(String departmentId, LocalDate startDate, LocalDate endDate);

    ReportStatusDto findAllByOperationIdIssueDateBetween(String operationId, LocalDate startDate, LocalDate endDate);

    ReportStatusDto findAllByDepartmentIdIssueDateBetween(String departmentId, LocalDate startDate, LocalDate endDate);

    ReceiveMoneySearchResponse receiveMoneyStatus(String mtcn, String operatorId);

    ReceiveMoneySearchResponse receiveMoneySearch(String mtcn, String operatorId, String departmentId);

    ReceiveMoneySearchResponse cancelMoneySearch(String mtcn, String operatorId);

    void receiveMoneyPay(TransferDto dto);

    void cancelMoneyPay(String mtcn, String operatorId);

    TransferDto searchDetails(String mtcn);
}
