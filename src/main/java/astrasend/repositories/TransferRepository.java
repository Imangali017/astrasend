package astrasend.repositories;

import astrasend.model.entity.Transfer;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> {
    Optional<Transfer> findByMtcn(String mtcn);
    void deleteByMtcn(String mtcn);
    List<Transfer> findAllByOperationIdAndPaidDateBetween(String operationId, LocalDateTime startDate, LocalDateTime endDate);
    List<Transfer> findAllByDepartmentIdAndPaidDateBetween(String departmentId, LocalDateTime startDate, LocalDateTime endDate);
}
