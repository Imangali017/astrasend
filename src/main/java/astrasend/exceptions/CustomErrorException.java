package astrasend.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomErrorException extends RuntimeException {
    private String code;
    private String message;

    public CustomErrorException(String code, String message) {
        this.message = message;
        this.code = code;
    }
}
