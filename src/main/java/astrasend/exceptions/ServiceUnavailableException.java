package astrasend.exceptions;

import astrasend.model.enums.Code;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceUnavailableException extends RuntimeException{

    Code code;

    public ServiceUnavailableException(Code code, String message) {
        super(message);
        this.code = code;
    }

}
