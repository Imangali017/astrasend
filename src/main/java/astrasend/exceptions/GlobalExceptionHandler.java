package astrasend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomErrorException.class)
    public ResponseEntity<Object> handleCustomErrorException(CustomErrorException ex) {
        String code = ex.getCode();
        String errorMessage = ex.getMessage();
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("timestamp", LocalDateTime.now());
        responseBody.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        responseBody.put("code", code);
        responseBody.put("error", HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        responseBody.put("message", errorMessage);
        responseBody.put("path", "/api/v1/astrasend");
        return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}


