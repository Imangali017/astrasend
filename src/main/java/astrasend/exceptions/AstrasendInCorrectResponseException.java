package astrasend.exceptions;

import astrasend.model.enums.Code;

public class AstrasendInCorrectResponseException extends RuntimeException {

    Code code;

    public AstrasendInCorrectResponseException(Code code, String message) {
        super(message);
        this.code = code;
    }


}
