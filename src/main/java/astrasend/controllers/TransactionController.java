package astrasend.controllers;

import astrasend.model.dtos.ReceiveMoneySearchResponse;
import astrasend.model.dtos.ReportStatusDto;
import astrasend.model.dtos.TransferDto;
import astrasend.services.impl.TransactionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("v1/astrasend")
public class TransactionController {

    private final TransactionServiceImpl transactionService;

    @Autowired
    public TransactionController(TransactionServiceImpl transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/receive-money-search-request")
    public ResponseEntity<?> receiveMoneySearch(@RequestParam String mtcn,
                                                         @RequestParam String operatorId,
                                                         @RequestParam String departmentId) {
        return ResponseEntity.ok(transactionService.receiveMoneySearch(mtcn, operatorId, departmentId));
    }

    @PostMapping("/receive-money-pay-request")
    public void payRequest(@RequestBody TransferDto dto) {
        transactionService.receiveMoneyPay(dto);
    }

    @PostMapping("/cancel-paid-request")
    public void cancelMoneySearch(@RequestParam String mtcn,
                                  @RequestParam String operatorId) {
        transactionService.cancelMoneyPay(mtcn, operatorId);
    }

    @PostMapping("/search-request")
    public ReceiveMoneySearchResponse cancelPay(@RequestParam String mtcn,
                                                @RequestParam String operatorId) {
        return transactionService.cancelMoneySearch(mtcn, operatorId);
    }

    @GetMapping("/status")
    public ReceiveMoneySearchResponse status(@RequestParam String mtcn,
                                             @RequestParam String operatorId) {
        return transactionService.receiveMoneyStatus(mtcn, operatorId);
    }

    @GetMapping("/detail")
    public TransferDto searchDetails(@RequestParam String mtcn) {
        return transactionService.searchDetails(mtcn);
    }

    @GetMapping("/report-by-operator")
    public ReportStatusDto reportByOperator(@RequestParam LocalDate startDate,
                                  @RequestParam LocalDate endDate,
                                  @RequestParam String operationId) {
        return transactionService.findAllByOperationIdIssueDateBetween(operationId, startDate, endDate);
    }

    @GetMapping("/report-by-department")
    public ReportStatusDto reportByDepartment(@RequestParam LocalDate startDate,
                                  @RequestParam LocalDate endDate,
                                  @RequestParam String departmentId) {
        return transactionService.findAllByDepartmentIdIssueDateBetween(departmentId, startDate, endDate);
    }

    @GetMapping("/download-report/department")
    public ResponseEntity<byte[]> downloadReport(@RequestParam LocalDate startDate,
                                                 @RequestParam LocalDate endDate,
                                                 @RequestParam String departmentId) {
        byte[] reportBytes = transactionService.generateReport(departmentId, startDate, endDate);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("filename", "report.xlsx");
        return new ResponseEntity<>(reportBytes, headers, HttpStatus.OK);
    }

}
